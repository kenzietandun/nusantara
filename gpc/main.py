#!/usr/bin/env python3

from plc_reader import read_plc
from typing import Dict

CONFIG_FILE = 'config.json'


def read_configuration(configuration_file: str) -> Dict[str, str]:
    # TODO: Implement me
    return {"Test": "123"}


def read_tags_configuration(configuration: Dict[str, str]) -> Dict[str, int]:
    # TODO: Implement me
    return {
        "H01_ETorque": 5,
        "H02_ETorque": 3,
        "H03_ETorque": 3,
        "H04_ETorque": 3,
        "H05_ETorque": 3,
        "H06_ETorque": 3,
        "H07_ETorque": 3,
        "H08_ETorque": 3,
    }


def read_plc_ip_address(configuration: Dict[str, str]) -> str:
    # TODO: Implement me
    return "192.168.108.150"


def read_read_strategy(configuration: Dict[str, str]) -> str:
    # TODO: Implement me
    return "random"


def main():
    # Setup
    app_configuration = read_configuration(CONFIG_FILE)
    tags_configuration = read_tags_configuration(app_configuration)
    plc_ip_address = read_plc_ip_address(app_configuration)
    plc_read_strategy = read_read_strategy(app_configuration)

    # Run the program
    print(
        read_plc(plc_ip_address,
                 tags_configuration,
                 strategy=plc_read_strategy))


if __name__ == "__main__":
    main()
