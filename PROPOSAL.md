# Nusantara

Solusi OSS (Open Source Software) untuk memantau kondisi mesin-mesin pabrik melalui protokol Ethernet/IP.

## Demo

Demo Nusantara dapat dilihat disini: https://nusantara.kenzietandun.com

## Software 

Projek ini menggunakan:
- Grafana https://github.com/grafana/grafana untuk web dashboard
- InfluxDB https://github.com/influxdata/influxdb untuk penyimpanan data
- Software racikan lokal untuk pengumpulan data

## Requirements

- Mesin dapat berkomunikasi dengan PLC
- PLC memiliki tags yang bisa di query melalui Ethernet/IP
- Koneksi ke internet

## Cara Kerja

Berikut adalah diagram sederhana, arah panah menunjukkan arah pergerakan data.

```
Mesin pabrik ---> PLC ---> Nusantara Collector ---> Nusantara Web ---> User
                       |
                       |
                       |
                    Melalui
                  Ethernet/IP
```

## Fitur

- Pemantauan untuk multi-site berbasis web
- Tampilan keseluruhan site dari satu dashboard
- Mobile friendly
- Alarm jika kondisi mesin melewati batas tertentu
- Notifikasi melalui SMS, Slack, Email

## Kontak

Silahkan hubungi saya melalui `nudnateiznek@gmail.com`.
