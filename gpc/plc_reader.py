#!/usr/bin/env python3

from abc import ABC, abstractmethod
from typing import Dict
import random

from cpppo.server.enip.get_attribute import proxy_simple


def read_plc(ip_address: str,
             tags: Dict[str, int],
             strategy="simple") -> Dict[str, float]:
    """Read the tags from the PLC specified located at ip address with the
    given strategy."""

    reader = None
    if strategy == "simple":
        reader = SimplePLCReader(ip_address)
    elif strategy == "random":
        reader = RandomValueReader(ip_address)

    return reader.read_plc(tags)


class ReaderException(Exception):
    def __init__(self):
        pass


class PLCReader(ABC):
    def __init__(self):
        ...

    @abstractmethod
    def read_plc(self, tags: Dict[str, int]) -> Dict[str, float]:
        pass


class SimplePLCReader(PLCReader):
    def __init__(self, plc_ip_address):
        self.plc_proxy = proxy_simple(plc_ip_address)

    def read_plc(self, tags: Dict[str, int]) -> Dict[str, float]:
        parameters = dict()
        for tag, _ in tags.items():
            parameters[tag] = (tag, "", None)

        try:
            params = self.plc_proxy.parameter_substitution(tags, parameters)
            values = self.plc_proxy.read(params)
            return {t: v[0] for t, v in zip(tags, values)}
        except Exception as e:
            self.plc_proxy.close_gateway(e)
            raise ReaderException()


class RandomValueReader(PLCReader):
    def __init__(self, plc_ip_address):
        self.plc_proxy = proxy_simple(plc_ip_address)

    def read_plc(self, tags: Dict[str, int]) -> Dict[str, float]:
        result = dict()
        for tag, _ in tags.items():
            result[tag] = random.randint(60, 80)
        return result
